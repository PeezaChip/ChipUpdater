﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ChipUpdater.Services
{
    public class UpdateService
    {
        private readonly UpdateOptions options;
        private Dictionary<string, Task<string>> UpdateTasks = new Dictionary<string, Task<string>>();

        public UpdateService(IOptions<UpdateOptions> options)
        {
            this.options = options.Value;
        }

        public Task<string> Update(string path, string key)
        {
            if (!options.Bots.ContainsKey(key)) throw new ArgumentException($"No bot with key '{key}' was found");
            if (UpdateTasks.ContainsKey(key)) return UpdateTasks[key];

            var task = Task.Run(() => RunUpdate(path, key));
            task.ContinueWith(t => { if (UpdateTasks.ContainsKey(key)) UpdateTasks.Remove(key); });
            UpdateTasks.Add(key, task);

            return task;
        }

        private string RunUpdate(string path, string key)
        {
            var bot = options.Bots[key];
            var p = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "bash",
                    Arguments = $"./Assets/deployBot.sh {bot.ServiceName} {bot.Path} {path} {bot.Executable}",
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                }
            };

            p.Start();
            p.WaitForExit();

            var output = p.StandardOutput.ReadToEnd() + "\n" + p.StandardError.ReadToEnd();

            if (p.ExitCode != 0)
            {
                throw new Exception(output);
            }
            else
            {
                return output;
            }
        }
    }

    public class UpdateOptions
    {
        public const string Key = "UpdateSettings";

        public Dictionary<string, BotSetting> Bots { get; set; }
    }

    public class BotSetting
    {
        public string ServiceName { get; set; }
        public string Path { get; set; }
        public string Executable { get; set; }
    }
}
