﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ChipUpdater.Controllers.Attributes
{
    public class GitLabWebhookAttribute : ActionFilterAttribute
    {
        private readonly ILogger<GitLabWebhookAttribute> logger;

        public static string apiToken;

        public GitLabWebhookAttribute(IConfiguration configuration, ILogger<GitLabWebhookAttribute> logger)
        {
            this.logger = logger;
            if (string.IsNullOrWhiteSpace(apiToken))
            {
                apiToken = configuration.GetValue<string>("ApiKey");
            }
        }

        public override void OnActionExecuting(ActionExecutingContext executingContext)
        {
            base.OnActionExecuting(executingContext);

            logger.LogInformation("Checking token");
            if (!executingContext.HttpContext.Request.Headers.TryGetValue("X-Gitlab-Token", out var token) || token != apiToken) { executingContext.Result = new NotFoundResult(); return; }
            logger.LogInformation("Token is correct");
        }
    }
}
