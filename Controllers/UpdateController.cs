﻿using ChipUpdater.Controllers.Attributes;
using ChipUpdater.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ChipUpdater.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UpdateController : ControllerBase
    {
        private readonly ILogger<UpdateController> logger;
        private readonly UpdateService updateService;

        public UpdateController(ILogger<UpdateController> logger, UpdateService updateService)
        {
            this.logger = logger;
            this.updateService = updateService;
        }

        [HttpPost("{key}")]
        [Produces("text/plain")]
        [Consumes("multipart/form-data")]
        [ServiceFilter(typeof(GitLabWebhookAttribute))]
        public async Task<IActionResult> Post(IFormFile file, string key)
        {
            try
            {
                logger.LogInformation($"Got request for updating {key}");

                if (!file.FileName.EndsWith(".tar.gz"))
                {
                    var msg = $"Expecting .tar.gz file, got {file.FileName}";
                    logger.LogInformation(msg);
                    return BadRequest(msg);
                }

                if (file.Length > 0)
                {
                    var filePath = Path.GetTempFileName();

                    using var stream = System.IO.File.Create(filePath);
                    await file.CopyToAsync(stream);
                    var result = await updateService.Update(filePath, key);
                    return Ok(result);
                }

                return BadRequest("File is empty");
            }
            catch(ArgumentException ex)
            {
                logger.LogInformation($"Update failed. {ex.Message}");
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                logger.LogInformation($"Update failed. {ex.Message}");
                return Problem(ex.Message);
            }
        }
    }
}
