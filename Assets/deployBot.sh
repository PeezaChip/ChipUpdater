﻿#!/bin/bash
set -e

echo changing dir to $2
cd $2

echo cleanup
rm -rf ./tmp

echo unzipping files
mkdir tmp
tar xf $3 -C tmp

echo stopping service $1
service $1 stop

echo removing old files
rm -rf ./Publish

echo moving new files
mv ./tmp ./Publish
chmod +x ./Publish/$4

echo starting service $1
service $1 start

echo cleanup
rm $3
rm -rf ./tmp
